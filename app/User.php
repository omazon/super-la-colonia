<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable,HasRoles;

    /**
     *
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','birthday',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function shoppinglists(){
    	return $this->belongsToMany('App\Shoppinglist');
    }
    public function owner_list(){
    	return $this->hasMany('App\Shoppinglist');
    }
}
