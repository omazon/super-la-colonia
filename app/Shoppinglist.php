<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sassnowski\LaravelShareableModel\Shareable\Shareable;
use Sassnowski\LaravelShareableModel\Shareable\ShareableInterface;

class Shoppinglist extends Model implements ShareableInterface
{
	use Shareable;
    protected $fillable = ['name','owner_id'];

    public function users(){
    	return $this->belongsToMany('App\User');
    }
    public function products(){
    	return $this->belongsToMany('App\Product','product_shoppinglist')->withPivot('product_account','product_status');
    }
    public function owner(){
    	return $this->belongsTo('App\User');
    }

}
