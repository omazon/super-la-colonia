<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$producto = Product::all();
        return view('product.index',['products'=>$producto]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$categories = Category::all()->pluck('name','id');
        return view('product.create',['categories'=>$categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = array(
        	'name.unique'=> 'El producto ya ha sido registrado'
        );
        $request->validate([
        	'name'=>'required|unique:products,name,NULL,id,deleted_at,null'
        ],$messages);
	    $product = Product::create(['name'=>ucfirst(mb_strtolower($request->name)),'category_id'=>$request->category_id]);
	    return redirect()->route('product.create')->with('success','El producto '.$product->name.' fue creada');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $categories = Category::all()->pluck('name','id');
        return view('product.edit',['product'=>$product,'categories'=>$categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    $messages = array(
		    'name.unique'=> 'El producto ya ha sido registrado'
	    );
	    $request->validate([
		    'name'=>'required|unique:products,name'
	    ],$messages);
	    $producto = Product::find($id);
	    $producto->name = ucfirst(mb_strtolower($request->name));
	    $producto->category_id = $request->category_id;
	    $producto->save();
	    return redirect()->route('product.index')->with('success','El producto fue actualiza');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::find($id)->delete();
        return redirect()->route('product.index')->with('success','El producto fue eliminado');
    }
    /*
    Lista de productos eliminados
    */
    public function deleted(){
	    $product = Product::onlyTrashed()->get();
	    return view('product.delete',['products'=>$product]);
    }
    public function restore_deleted($id){
    	Product::withTrashed()->where('id',$id)->restore();
	    return redirect()->route('product.deleted')->with('success','El producto fue restaurado');
    }
    public function delete_permanent($id){
	    Product::withTrashed()->where('id',$id)->forceDelete();
	    return redirect()->route('product.deleted')->with('success','El producto fue eliminado completamente');
    }
}
