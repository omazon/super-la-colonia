<?php

namespace App\Http\Controllers;

use App\Product;
use App\Shoppinglist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Sassnowski\LaravelShareableModel\Shareable\Shareable;
use Sassnowski\LaravelShareableModel\Shareable\ShareableLink;

class ShoppinglistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$shoppinglist = Shoppinglist::where('owner_id',Auth::id())->get();
        return view('shoppinglist.index',['shoppinglist'=>$shoppinglist]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$products= Product::all();
        return view('shoppinglist.create',['products'=>$products]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	    $shoppinglist = Shoppinglist::create([
	    	'name'=>ucfirst(mb_strtolower($request->titulo)),
		    'owner_id'=>Auth::id()
	    ]);
	    $i = 0;
	    foreach ($request->product_id as $product_id){
		    $shoppinglist->products()->attach($product_id,['product_account'=>$request->product_account[$i]]);
		    $i++;
	    }
//	    Shareable::buildFor($shoppinglist)->setActive()->build();
	    return response()->json(array('success'=>'Lista ha sido creada'));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $shoppinglist = Shoppinglist::where('owner_id',Auth::id())->findOrFail($id);
        $link = ShareableLink::buildFor($shoppinglist)->setActive()->setPrefix('shoppinglist')->build();
        return view('shoppinglist.show',['shoppinglist'=>$shoppinglist, 'link'=>$link]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$products = Product::all();
	    $shoppinglist = Shoppinglist::where('owner_id',Auth::id())->findOrFail($id);
	    return view('shoppinglist.edit',['products'=>$products,'shoppinglist'=>$shoppinglist]);
    }

    public function shopping($id){
	    $products = Product::all();
	    $shoppinglist = Shoppinglist::where('owner_id',Auth::id())->findOrFail($id);
	    return view('shoppinglist.shopping',['products'=>$products,'shoppinglist'=>$shoppinglist]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    $shoppinglist = Shoppinglist::where('owner_id',Auth::id())->findOrFail($id);
	    $shoppinglist->name = $request->titulo;
	    $shoppinglist->save();

	    $i = 0;
	    foreach($request->product_id as $product_id){
	    	$pivot_data = [
	    		'product_account' => $request->product_account[$i],
			    'product_id' => $request->product_id[$i]
		    ];
			$data_to_sync[$product_id] = $pivot_data;
	    	$i++;
	    }
	    $shoppinglist->products()->sync($data_to_sync);
	    return response()->json(array('success'=>'Lista ha sido actualizada'));
    }

    public function update_shopping(Request $request,$id){
	    $shoppinglist = Shoppinglist::where('owner_id',Auth::id())->findOrFail($id);
	    foreach ($request->bought as $bought){
	    	$shoppinglist->products()->updateExistingPivot($bought['iden'],['product_status' => $bought['status']]);
	    }
	    return response()->json(array('success'=>'Has terminado de comprar'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
	    $shopping = Shoppinglist::where('owner_id',Auth::id())->findOrFail($id);
	    $shopping->products()->detach();
	    $shopping->delete();
	    return redirect()->route('shoppinglist.index')->with('success','El producto fue eliminado');
    }

    public function shared_link($link){
    	if(Auth::guest()){
    		session()->put('shared_link',$link);
    		return redirect('login');
	    }
    	return $link->shareable;
    }
}
