<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//RUTAS DE LIstas COMPARTIDOS
Route::group(['middleware'=>'shared'],function (){
	Route::get('shared/shoppinglist/{shareable_link}','ShoppinglistController@shared_link');
});

//RUTAS DE USUARIO
Route::group(['middleware'=>'auth'],function (){
	Route::resource('shoppinglist','ShoppinglistController');
	Route::get('edit/{id}/shopping','ShoppinglistController@shopping')->name('shoppinglist.shopping');
	Route::put('/shopping/{id}','ShoppinglistController@update_shopping')->name('shoppinglist.update');
});

//RUTAS DE ADMINISTRADOR
Route::group(['prefix' => 'admin'], function () {
    Route::group(['middleware'=>'auth:admin'],function (){
        Route::get('/register', 'AdminAuth\RegisterController@showRegistrationForm')->name('admin.register');
        Route::post('/register', 'AdminAuth\RegisterController@register');
        Route::resource('category','CategoryController');
		Route::get('product/deleted','ProductController@deleted')->name('product.deleted');
		Route::patch('product/deleted/{id}','ProductController@restore_deleted')->name('product.restore');
		Route::delete('product/deleted/{id}','ProductController@delete_permanent')->name('product.perma_delete');
		Route::resource('product','ProductController');
	});

    //RUTAS DE LOGIN ADMIN
  Route::get('/login', 'AdminAuth\LoginController@showLoginForm')->name('admin.login');
  Route::post('/login', 'AdminAuth\LoginController@login');
  Route::post('/logout', 'AdminAuth\LoginController@logout')->name('admin.logout');


//RUTAS DE CONTRASEÑA ADMIN
  Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.request');
  Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset')->name('admin.password.email');
  Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm')->name('admin.password.reset');
  Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');
});
