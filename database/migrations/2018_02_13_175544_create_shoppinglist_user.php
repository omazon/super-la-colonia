<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShoppinglistUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shoppinglist_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shoppinglist_id')->unsigned();
            $table->foreign('shoppinglist_id')->references('id')->on('shoppinglists');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shoppinglist_user', function (Blueprint $table) {
            //
        });
    }
}
