@extends('admin.layout.auth')

@section('content')
<div class="container">
    <div class="row mt-5">
        <div class="col-md-8 offset-md-2">
            <div class="card">
                <div class="card-header d-flex align-items-center justify-content-between">Categorias
                    <a href="{{route('category.create')}}" class="btn btn-success">Agregar</a>

                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <thead>
                        <th>Nombre</th>
                        </thead>
                        <tbody>
                        @foreach($categories as $category)
                            <tr>
                                <td>{{$category->name}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
