@extends('admin.layout.auth')

@section('content')
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-8 offset-md-2">
                @if($message = Session::get('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <span>{{$message}}</span>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                <div class="card">
                    <div class="card-header">Crear nueva categoría</div>
                    <div class="card-body">
                        {!! Form::open(['route'=> 'category.store']) !!}
                        <div class="form-group">
                            {{Form::text('name',null,['placeholder'=>'Ingrese el nombre de la categoría','class'=>'form-control','required'])}}
                        </div>
                            {{Form::submit('Guardar',['class'=>'btn btn-primary'])}}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
