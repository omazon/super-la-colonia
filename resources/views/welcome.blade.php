@extends('layouts.home')
@section('content')
    <section class="body">
        <img class="w-50 d-block mx-auto" src="{{asset('/images/Elementos-11.png')}}" alt="">
        <div class="container">
            <p class="text-center p-100">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi at dignissimos laborum nisi veniam!</p>
            <button class="btn btn-danger btn-lg d-flex mx-auto">Crea tu Shop List</button>
        </div>
    </section>
@endsection