@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-8 offset-md-2">
                @if(session('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <span>{{session('success')}}</span>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                <div class="card">

                    <div class="card-header d-flex flex-column align-items-center justify-content-between">
                        <span>Mis listas - <b>{{$shoppinglist->name}}</b></span>
                        <div class="container-fluid">
                            <a href="{{route('shoppinglist.create')}}" class="btn btn-success btn-block">Agregar nueva lista</a>
                            <a href="{{route('shoppinglist.index')}}" class="btn btn-info btn-block">Ver mis listas</a>
                            <a href="{{route('shoppinglist.edit',$shoppinglist->id)}}" class="btn btn-primary btn-block">Editar esta lista</a>
                            <a href="{{route('shoppinglist.shopping',$shoppinglist->id)}}" class="btn btn-warning btn-block">Comprar con esta lista</a>
                            {!! Form::open(['method'=>'DELETE','route'=>['shoppinglist.destroy',$shoppinglist->id],'class'=>'delete']) !!}
                            {!! Form::submit('Borrar',['class'=>'btn btn-danger btn-block delete','style'=>'margin-top:0.5rem']) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover">
                            <div class="d-flex flex-column">
                                <span>Fecha de creación: <b>{{$shoppinglist->created_at->format('d-m-Y g:i:s a')}}</b> ({{$shoppinglist->created_at->diffForHumans()}})</span>
                                <span>Creado por: <b>{{$shoppinglist->owner->name}}</b></span>
                                <span>Link para compartir <b>{{$link->url}}</b></span>
                            </div>
                            <thead>
                            <tr>
                                <th>Lista de productos</th>
                                <th>Categoría</th>
                                <th>Cantidad</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($shoppinglist->products as $list)
                                <tr :class="{'table-success' : {{$list->pivot->product_status}}== 1}">
                                    <td>{{$list->name}}</td>
                                    <td>{{$list->category->name}}</td>
                                    <td>{{$list->pivot->product_account}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
      $(".delete").on("submit", function(){
        return confirm("¿Realmente quieres eleminar este elemento?");
      });
    </script>
@endsection