@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-8 offset-md-2">
                @if(session('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <span>{{session('success')}}</span>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                <div class="card">
                    <div class="card-header d-flex align-items-center justify-content-between">Mis listas
                        <a href="{{route('shoppinglist.create')}}" class="btn btn-success">Agregar nueva lista</a>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover">
                            <thead>
                            <th>Nombre</th>
                            <th>Fecha de creación</th>
                            <th>Acciones</th>
                            </thead>
                            <tbody>
                            @foreach($shoppinglist as $list)
                                <tr>
                                    <td>{{$list->name}}</td>
                                    <td>{{$list->created_at->format('d-m-Y g:i:s a')}}</td>
                                    <td>
                                        <div class="d-flex flex-wrap">
                                            <a href="{{route('shoppinglist.show',$list->id)}}" class="btn btn-primary" role="button">Ver detalles</a>
                                            {!! Form::open(['method'=>'DELETE','route'=>['shoppinglist.destroy',$list->id],'class'=>'delete']) !!}
                                            {!! Form::submit('Borrar',['class'=>'btn btn-danger']) !!}
                                            {!! Form::close() !!}
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
      $(".delete").on("submit", function(){
        return confirm("¿Realmente quieres eleminar este elemento?");
      });
    </script>
@endsection