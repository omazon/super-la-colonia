@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-8 offset-md-2">
                @if(session('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <span>{{session('success')}}</span>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                <div class="card">
                    <div class="card-header d-flex align-items-center justify-content-between">Productos Eliminados
                        <a href="{{route('product.index')}}" class="btn btn-success">Lista de productos</a>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover">
                            <thead>
                            <th>Nombre</th>
                            <th>Categoría</th>
                            <th>Acción</th>
                            </thead>
                            <tbody>
                            @foreach($products as $product)
                                <tr>
                                    <td>{{$product->name}}</td>
                                    <td>{{$product->category->name}}</td>
                                    <td class="d-flex flex-row justify-content-around">
                                        {!! Form::open(['route'=> ['product.restore',$product->id],'method'=>'PATCH']) !!}
                                        {!! Form::submit('Restaurar',['class'=>'btn btn-info']) !!}
                                        {!! Form::close() !!}
                                        {!! Form::open(['method'=>'DELETE','route'=>['product.perma_delete',$product->id]]) !!}
                                        {!! Form::submit('Borrar Permanente',['class'=>'btn btn-danger']) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
