@extends('admin.layout.auth')

@section('content')
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-8 offset-md-2">
                @if(session('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <span>{{session('success')}}</span>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                <div class="card">
                    <div class="card-header d-flex align-items-center justify-content-between">Productos
                        <a href="{{route('product.create')}}" class="btn btn-success">Agregar</a>
                        <a href="{{route('product.deleted')}}" class="btn btn-warning">Productos borrados</a>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover">
                            <thead>
                            <th>Nombre</th>
                            <th>Categoría</th>
                            <th>Acción</th>
                            </thead>
                            <tbody>
                            @foreach($products as $product)
                                <tr>
                                    <td>{{$product->name}}</td>
                                    <td>{{$product->category->name}}</td>
                                    <td class="d-flex flex-row justify-content-around">
                                        <a href="{{route('product.edit',$product->id)}}" class="btn btn-info" role="button">Editar</a>
                                        {!! Form::open(['method'=>'DELETE','route'=>['product.destroy',$product->id]]) !!}
                                        {!! Form::submit('Borrar',['class'=>'btn btn-danger']) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
